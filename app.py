import datetime
import json
import os

from flask import Flask, jsonify, request, abort, make_response
from flask_sqlalchemy import SQLAlchemy

from utils import pow

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("SQLALCHEMY_DATABASE_URI", "sqlite:///test.sqlite3")
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)

class Pow_service(db.Model):
    __tablename__ = 'pow_service'

    id = db.Column(db.Integer, primary_key=True)
    base = db.Column(db.Integer, index=False, unique=False, nullable=False)
    exponent = db.Column(db.Integer, index=False, unique=False, nullable=False)
    modulus = db.Column(db.Integer, index=False, unique=False, nullable=False)
    created = db.Column(db.DateTime(), index=True, unique=False, nullable=False)

    def __repr__(self):
        return f'id {self.id}'

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Resource Not found'}), 404)

@app.route("/pow/api/v1.0/calculate", methods=['GET'])
def pow_calculate():
    if not request.json \
            or not 'base' in request.json \
            or not 'exponent' in request.json:
        abort(404)
    base = json.loads(request.json['base'])
    exponent = json.loads(request.json['exponent'])
    modulus = json.loads(request.json.get('modulus')) if 'modulus' in request.json else None
    power = pow(base, exponent, modulus)
    try:
        new_pow = Pow_service(
            base = base,
            exponent = exponent,
            modulus = modulus if modulus else 0,
            created = datetime.datetime.now(),
        )
        db.session.add(new_pow)
        db.session.commit()
    except:
        print('something went wrong')
        abort(400)
    result = {
        "operation": "pow",
        "base": base,
        "exponent": exponent,
        "modulus": modulus,
        "result": power,
    }
    return jsonify(result)

@app.route("/pow/api/v1.0/results", methods=['GET'])
def pow_results():
    pows = Pow_service.query.all()
    # result = list()
    result = dict()
    for item in pows:
        result[item.id] = {
            "Base": item.base,
            "Exponent": item.exponent,
            "Modulus": item.modulus,
            "Created": item.created
        }
    return jsonify(result)

@app.route("/")
def home():
    return "Work in progress. Contact admin."

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
