


def pow(base, exponent, modulus=None):
    return base ** exponent if modulus is None else base ** exponent % modulus
